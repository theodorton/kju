require 'kju/cluster'

describe Kju::Cluster do
  shared_context :several_objects do
    before(:each) do
      subject.add(:foo)
      subject.add(:bar)
    end
  end

  describe "adding and popping a objects" do
    it "should be able to add and pop (retrieve) an object" do
      subject.add(:foobar)
      subject.pop.should eq :foobar
    end

    it "should be possible to add and pop several objects" do
      subject.add(:foo)
      subject.add(:bar)

      subject.pop.should eq :foo
      subject.pop.should eq :bar
    end
  end

  describe "counting objects" do
    include_context :several_objects

    it "should be possible to add several objects and get a count of objects" do
      subject.count.should eq 2
    end
  end

  describe "prioritizing objects" do
    it "added objects should have a default priority 1" do
      subject.add(:foo)
      subject.pop.priority.should eq 1
    end

    it "should be able to add object with a priority" do
      subject.add(:foo, 5)
      subject.pop.priority.should eq 5
    end

    it "should only accept numbers (or numberish)" do
      expect {
        subject.add(:foo, 1)
      }.not_to raise_error

      expect {
        subject.add(:foo, '1')
      }.not_to raise_error

      expect {
        subject.add(:foo, 'A')
      }.to raise_error
    end

    describe "a simple queue" do
      before(:each) do
        subject.add(:foo)
        subject.add(:bar)
        subject.add(:boo)
        subject.add(:far)
      end

      its(:ordered_objects) { should eq [:foo, :bar, :boo, :far] }
    end

    describe "returning prioritized objects" do
      it "returns a prioritized object based on waiting time" do
        subject.add(:foo, 1)
        subject.add(:bar, 1)
        subject.add(:fiz, 2)
        subject.add(:woo, 1)
        subject.add(:wah, 1)

        subject.pop.should eq :foo
        subject.pop.should eq :fiz
        subject.pop.should eq :bar
        subject.pop.should eq :woo
        subject.pop.should eq :wah
      end
    end
  end
end
