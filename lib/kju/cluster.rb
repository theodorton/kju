require 'delegate'

module Kju
  class Cluster
    def initialize
      @line_number      = 0
      @next_line_number = 0
    end

    def add(object, priority = 1)
      objects.unshift ClusterObject.new(object, priority, next_line_number)
    end

    def pop
      objects.each(&:wait!)
      ordered_objects.shift
    end

    def ordered_objects
      objects.sort_by! do |obj|
        obj.wait_time
      end
      objects.reverse!
      objects
    end

    def count
      objects.size
    end

    def next_line_number
      @line_number += 1
    end

    private 

    def objects
      @objects ||= []
    end

    class ClusterObject < SimpleDelegator
      attr_accessor :priority, :line_number, :wait_time

      def initialize(object, priority, line_number)
        @priority    = Integer(priority)
        @line_number = Integer(line_number)
        @wait_time   = 0-@line_number
        super(object)
      end

      def wait!
        @wait_time += @priority
      end
    end
  end
end
